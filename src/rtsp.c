/*
 * =====================================================================================
 *
 *       Filename:  rtsp.c
 *
 *    Description:  This file implements the RTSP functions
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>

#include "util.h"
#include "tcp.h"
#include "rtsp.h"

struct rtsp_msg rtsp_parse(char *buf, int size)
{
	assert(buf != NULL);
	assert(size > 0);

	char *buf_begin, *buf_end; 
	char *line_end, *msg_end;
	struct rtsp_msg msg;

	// init
	msg.type = RTSP_TYPE_INVALID;
	buf_end = &buf[size - 1];
	for (buf_begin = buf; isspace(*buf_begin); buf_begin++); // skip white space

	// if buffer contains a full msg
	msg_end = rtsp_end_of_msg(buf_begin, buf_end - buf_begin + 1);
	if (NULL == msg_end)
		return msg;

	msg.length = msg_end - buf + 1;

	line_end = rtsp_end_of_line(buf_begin, buf_end - buf_begin + 1);
	/* should not be too long */
	if (line_end - buf_begin > URL_LEN_MAX + 50) return msg;

	/* find type and method */
	char first[URL_LEN_MAX + 50];
	char second[URL_LEN_MAX + 50];
	char third[URL_LEN_MAX + 50];
	int element_cnt;
	int reserved;

	reserved = *(line_end - 1);
	*(line_end - 1) = '\0';
	element_cnt = sscanf(buf_begin, "%s %s %s", first, second, third);
	*(line_end - 1) = reserved;

	if (element_cnt != 3) return msg;

	if (strcmp(first, RTSP_TOKEN) == 0) { // RTSP reply
		msg.type = RTSP_TYPE_REPLY;
		return msg;
	}
	else if (strcmp(third, RTSP_TOKEN) == 0) { // RTSP request
		if (strcmp(first, "OPTIONS") == 0) 
			msg.method = RTSP_METHOD_OPTIONS;
		else if (strcmp(first, "DESCRIBE") == 0)
			msg.method = RTSP_METHOD_DESCRIBE;
		else if (strcmp(first, "SETUP") == 0) 
			msg.method = RTSP_METHOD_SETUP;
		else if (strcmp(first, "PLAY") == 0)
			msg.method = RTSP_METHOD_PLAY;
		else if (strcmp(first, "PAUSE") == 0)
			msg.method = RTSP_METHOD_PAUSE;
		else if (strcmp(first, "GET_PARAMETER") == 0) 
			msg.method = RTSP_METHOD_GET_PARAMETER;
		else if (strcmp(first, "TEARDOWN") == 0) 
			msg.method = RTSP_METHOD_TEARDOWN;
		else
			msg.method = RTSP_METHOD_INVALID;

		if (msg.method != RTSP_METHOD_INVALID) {
			msg.type = RTSP_TYPE_REQUEST;
			strcpy(msg.url, second);
		}
		return msg;

	}
	return msg;
}

char *rtsp_end_of_line(const char *buf, int size)
{
	assert(buf != NULL);
	assert(size > 0);

	char *pos;

	pos = memchr(buf, '\r', size);
	pos++;
	// exceed buffer size
	if (pos - buf < size && *pos == '\n') 
		return pos;	
	return NULL;
}

static char *_pos_crln_crln(char *begin, char *end) 
{
	char *line_end, *from, *to;
	from = begin;
	to = end;

	while(from && to && from < to) {
		line_end = rtsp_end_of_line(from, to - from + 1);
		if (line_end == NULL || to - line_end < 2) {
			return NULL;
		}
		if (*(line_end + 1) == '\r' && *(line_end + 2) == '\n') {
			return line_end + 2;
		}
		from = line_end + 1;
	}
	return NULL;
}

char *rtsp_end_of_msg(char *buf, int size)
{
	assert(buf != NULL);
	assert(size > 0);

	char *head_end, *msg_end, *buf_end;

	buf_end = buf + size - 1;
	head_end = _pos_crln_crln(buf, buf_end);

	if (head_end != NULL) {
		int reserved;
		char *len_pos;
		int body_len;
		int res;
		reserved = *(head_end - 1);
		*(head_end - 1) = '\0';
		len_pos = strstr(buf, "Content-Length");
		if (len_pos == NULL) {// no body
			*(head_end - 1) = reserved;
			return head_end;
		}
		body_len = -1;
		res = sscanf(len_pos, "Content-Length : %d", &body_len);
		*(head_end - 1) = reserved;
		if (res == 1 && body_len > 0) { // have body
			msg_end = head_end + body_len;
			return msg_end;
		}
	}

	return NULL;
			
}
