/*
 * =====================================================================================
 *
 *       Filename:  tcp.h
 *
 *    Description:  This file defines the TCP functions
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */


#ifndef _TCP_H_
#define _TCP_H_

#define TCP_BUF_SIZE_MAX 4096

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  tcp_connect
 *  Description:  Establish a TCP connection
 * =====================================================================================
 *   Parameters:  
 *                host -> hostname or ip address
 *                port -> port
 *      Returns:  
 *                socket of TCP connection, -1 on failure
 * =====================================================================================
 */
int tcp_connect(const char *host, int port);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  tcp_recv
 *  Description:  Read message from TCP socket
 * =====================================================================================
 *   Parameters:  
 *                sock     -> socket for reading
 *                buf      -> pointer to msg buffer
 *                max_size -> size of buffer, should not exceed TCP_BUF_SIZE_MAX
 *      Returns:  
 *                size of msg received, <= 0 on failure
 * =====================================================================================
 */
int tcp_recv(int sock, char *buf, int max_size);

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  tcp_send
 *  Description:  Send message to TCP socket
 * =====================================================================================
 *   Parameters:  
 *                sock -> socket for reading
 *                buf  -> pointer to msg buffer
 *                size -> number of byte to send
 *      Returns:  
 *                1 on success, 0 on failure
 * =====================================================================================
 */
int tcp_send(int sock, char *buf, int size);

#endif
