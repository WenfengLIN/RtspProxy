/*
 * =====================================================================================
 *
 *       Filename:  util.c
 *
 *    Description:  This file implements the util functions
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <getopt.h>
#include <assert.h>

#include "util.h"

int gDebug;

struct configuration util_parse_arg (int argc, char *argv[])
{
	int opt;
	struct configuration config;

	// Default virtual RTSP server -> localhost:554
	strcpy(config.sim_host, "localhost");
	config.sim_port = 554;

	gDebug = FALSE;

	while ((opt = getopt(argc, argv, "s:p:hd")) != -1) {
		switch (opt) {
		case 'h': // help
			util_print_help(argv[0]);
			exit(0);
		case 'd': // debug mode
			gDebug = TRUE;
			break;
		case 's': // virtual hostname
			if (strlen(optarg) >= HOST_NAME_MAX_LENGTH) {
				util_log ("util_parse_arg(), virtual hostname too long");
				util_print_help(argv[0]);
				exit(0);
			}
			strcpy(config.sim_host, optarg);
			break;
		case 'p': // virtual port
			config.sim_port = atoi(optarg);
			if (config.sim_port < 0) {
				util_log ("util_parse_arg(), invalid virtual port");
				util_print_help(argv[0]);
				exit(0);
			}
			break;
		default:
			util_log ("util_parse_arg(), unknown arguments");
			util_print_help(argv[0]);
			exit(0);
		}
	}
	if (optind + 4 > argc) {
		util_log ("util_parse_arg(), expected more arguments");
		util_print_help(argv[0]);
		exit(0);
	}
	// camera hostname
	if (strlen(argv[optind]) >= HOST_NAME_MAX_LENGTH) {
		util_log ("util_parse_arg(), camera hostname too long");
		util_print_help(argv[0]);
		exit(0);
	}
	strcpy(config.camera_host, argv[optind]);
	optind++;
	// camera port
	config.camera_port = atoi(argv[optind]);
	if (config.camera_port < 0) {
		util_log ("util_parse_arg(), invalid camera port");
		util_print_help(argv[0]);
		exit(0);
	}
	optind++;
	// cloud hostname
	if (strlen(argv[optind]) >= HOST_NAME_MAX_LENGTH) {
		util_log ("util_parse_arg(), cloud server hostname too long");
		util_print_help(argv[0]);
		exit(0);
	}
	strcpy(config.cloud_host, argv[optind]);
	optind++;
	// cloud server port
	config.cloud_port = atoi(argv[optind]);
	if (config.cloud_port < 0) {
		util_log ("util_parse_arg(), invalid cloud server port");
		util_print_help(argv[0]);
		exit(0);
	}

	if (gDebug) {
		fprintf(
			stdout, "[Debug] Configuration:\n[Debug]\tCamera -> %s:%d\n[Debug]\tCloud -> %s:%d\n[Debug]\tVirtual -> %s:%d\n",
			config.camera_host,config.camera_port,
			config.cloud_host, config.cloud_port, 
			config.sim_host, config.sim_port
		);
	}

	return config;
}

void util_print_help (const char *progname)
{
	fprintf(stdout, "Usage: %s [-s virtual_hostname] [-p virtual_port] camera_host camera_port cloud_host cloud_port\n", progname);
	fprintf(stdout, "Options:\n");
	fprintf(stdout, "\t-h\tprint this help message\n");
	fprintf(stdout, "\t-d\tturn on debug mode\n");
	fprintf(stdout, "\t-s\thostname of virtual RTSP server, default \"localhost\"\n");
	fprintf(stdout, "\t-p\tport of virtual RTSP server, default 554\n");
}

void util_log (const char *msg)
{
	assert(msg != NULL);
	time_t t;
	struct tm *tmp;
	char time_str[50];

	t = time(NULL);
	tmp = localtime(&t);
	if (tmp == NULL) {
		util_error("util_time_str() -> localtime()");
	}
	if (strftime(time_str, sizeof(time_str), "%Y-%m-%d %H:%M:%S", tmp) == 0) {
		util_error("util_time_str() -> strftime()");
	}

	fprintf(stdout, "[INFO] (%s) %s\n", time_str, msg);
}

void util_error (const char *msg)
{
	assert(msg != NULL);
	fprintf(stderr, "[ERROR] %s\n", msg);
	exit(1);
}
