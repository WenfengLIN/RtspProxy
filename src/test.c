/*
 * =====================================================================================
 *
 *       Filename:  test.c
 *
 *    Description:  The program performs unit tests
 *
 *        Version:  1.0
 *        Created:  01/09/2017
 *       Revision:  01/11/2017
 *
 *         Author:  Wenfeng LIN
 *          Email:  wf.lin@clobotics.com
 *        Company:  Clobotics
 *
 * =====================================================================================
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/select.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>

#include "util.h"
#include "tcp.h"
#include "rtsp.h"

void test_util (void) 
{
	util_log("If you can see this message, both util_time_str() and util_log() work");
	util_error("If you can also see this message, util_error() works\nAnd I quit now");
}

void test_tcp (void)
{
	char host[50];
	char buf[TCP_BUF_SIZE_MAX];
	int port;

	strcpy(host, "192.168.5.43");
	port = 554;

	int res = tcp_connect(host, port);
	if (res < 0)
		util_error("tcp_connect() failed");
	else
		util_log("tcp_connect() works");
	int rdnb = tcp_recv (res, buf, TCP_BUF_SIZE_MAX);
	if (rdnb > 0)  {
		util_log("tcp_recv() works");
		int sdnb = tcp_send (res, buf, rdnb);
		if (sdnb > 0) 
			util_log("tcp_send() works");
		else
			util_log("tcp_send() failed");
	}
	else
		util_log("tcp_recv() failed");
	close(res);
}

void test_rtsp(void)
{
	char reply[200] = "RTSP/1.0 200 OK\r\nCseq: 1\r\n\r\n";
	char request[200] = "OPTIONS locolhost:500 RTSP/1.0\r\nCseq: 1\r\nContent-Length: 50\r\n\r\nyes\r\n\r\n";
	char random[200] = "GET / HTTP/1.1\r\nHost baidu.com\r\n\r\n";

	struct rtsp_msg msg;
	msg = rtsp_parse(reply, strlen(reply) + 1);
	if (msg.type == RTSP_TYPE_INVALID) 
		util_log("Unknown msg");
	else {
		printf ("Msg: type -> %d, method -> %d, url -> %s\n", msg.type, msg.method, msg.url);
	}
	msg = rtsp_parse(request, strlen(request) + 1);
	if (msg.type == RTSP_TYPE_INVALID) 
		util_log("Unknown msg");
	else {
		printf ("Msg: length -> %d (%ld), type -> %d, method -> %d, url -> %s\n", msg.length, strlen(request), msg.type, msg.method, msg.url);
	}
	msg = rtsp_parse(random, strlen(random) + 1);
	if (msg.type == RTSP_TYPE_INVALID) 
		util_log("Unknown msg");
	else {
		printf ("Msg: type -> %d, method -> %d, url -> %s\n", msg.type, msg.method, msg.url);
	}
}

int main (int argc, char *argv[])
{
	//test_util();
	//test_tcp();
	test_rtsp();
	return 0;
}
